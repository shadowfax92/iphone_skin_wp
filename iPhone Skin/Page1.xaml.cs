﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.IO.IsolatedStorage;

namespace iPhone_Skin
{
    public partial class Page1 : PhoneApplicationPage
    {
        IsolatedStorageSettings settings1 = IsolatedStorageSettings.ApplicationSettings;
        PhoneNumberChooserTask phoneNumberChooserTask;
        public Page1()
        {
            
            InitializeComponent();
            
        }
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            string first_time = "";
            if (!settings1.Contains("first_time"))
            {
                MessageBox.Show("Not all icons are working as of now. Some icons like \"Notes\", \"Voice Recorder\" and others are not working. We are working very hard to make them work. Please cooperate with us!");
                settings1["first_time"] = "first_time_done";

            }//end if
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }


        private void GestureListener_Flick(object sender, FlickGestureEventArgs e)
        {
            //ignore if direction is not horizontal
            if (e.Direction != System.Windows.Controls.Orientation.Horizontal)
                return;

            
            //right-to-left
            if (e.HorizontalVelocity < 0)
            {
                //MessageBox.Show("right to left");
                string uriString = "/Page2.xaml";
                //String uriString = "Uri=/iPhone_Skin;component/Page2.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));

            }

            //left-to-right
            if (e.HorizontalVelocity > 0)
            {
                //MessageBox.Show("left to right");
                string uriString = "/Page3.xaml";
                //String uriString = "Uri=/iPhone_Skin;component/Page3.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));
            }
        }

        private void Button_clicked(object sender, RoutedEventArgs e)
        {
            Button current_button = (Button)sender;
            String button_name = current_button.Name;
            switch (button_name)
            {
                case "message":
                    //MessageBox.Show("message button clicked");
                    SmsComposeTask sms = new SmsComposeTask();
                    sms.To = "";
                    sms.Show();
                    break;
                case "calendar":
                    //MessageBox.Show("Canlendar button clicked");
                    WebBrowserTask webBrowserTask1 = new WebBrowserTask();
                    webBrowserTask1.Uri = new Uri("http://calendar.google.com/", UriKind.Absolute);
            webBrowserTask1.Show();
                    break;
                case "photos":
                     PhotoChooserTask photo = new PhotoChooserTask();
            photo.Show();
                    break;
                case "camera":
                    CameraCaptureTask cam = new CameraCaptureTask();
                    cam.Show();
                    break;
                case "youtube":
                     WebBrowserTask webBrowserTask2 = new WebBrowserTask();
                     webBrowserTask2.Uri = new Uri("http://m.youtube.com/", UriKind.Absolute);
            webBrowserTask2.Show();
                    break;
                case "stocks":
                    break;
                case "maps":
                    //MessageBox.Show("maps clicked");
                    BingMapsTask bmt = new BingMapsTask();
            //bmt.Center = new GeoCoordinate(-27.5, 153);
            bmt.SearchTerm = "New York";
            bmt.Show();
                    break;
                case "weather":
                    WebBrowserTask webBrowserTask3 = new WebBrowserTask();
                    webBrowserTask3.Uri = new Uri("http://www.weather.com/", UriKind.Absolute);
            webBrowserTask3.Show();
                
                    break;
                case "notes":
                    break;
                case "voice_memos":
                    break;
                case "itunes":
                     MarketplaceHubTask market1 = new MarketplaceHubTask();
                    market1.Show();
                    break;
                case "app_store":
                     MarketplaceHubTask market = new MarketplaceHubTask();
                    market.Show();
                    break;
                case "settings":
                    break;
                case "phone":
                    phoneNumberChooserTask = new PhoneNumberChooserTask();
                    phoneNumberChooserTask.Completed += new EventHandler<PhoneNumberResult>(phoneNumberChooserTask_Completed);
                    try
                    {
                        phoneNumberChooserTask.Show();
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        MessageBox.Show("An error occurred.");
                    }
                    //PhoneCallTask phoneCallTask = new PhoneCallTask();

                    //phoneCallTask.PhoneNumber = "123456789";
                    //phoneCallTask.DisplayName = "Shadowfax Apps";

                    //phoneCallTask.Show();
                    break;
                case "mail":
                    EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "";
            emailComposeTask.Body = "";
            emailComposeTask.To = "";
            emailComposeTask.Show();
                    break;
                case "safari":
                    WebBrowserTask webBrowserTask4 = new WebBrowserTask();
            webBrowserTask4.Uri = new Uri("http://www.google.com", UriKind.Absolute);
            webBrowserTask4.Show();
                    break;
                case "ipod":
                    
                    break;
                    
                default:
                    break;
            }
        }
        void phoneNumberChooserTask_Completed(object sender, PhoneNumberResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                MessageBox.Show("The phone number for " + e.DisplayName + " is " + e.PhoneNumber);

                //Code to start a new call using the retrieved phone number.
                PhoneCallTask phoneCallTask = new PhoneCallTask();
                phoneCallTask.DisplayName = e.DisplayName;
                phoneCallTask.PhoneNumber = e.PhoneNumber;
                phoneCallTask.Show();
            }
        }

        
        
    }
}