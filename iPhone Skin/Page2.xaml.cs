﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace iPhone_Skin
{
    public partial class Page2 : PhoneApplicationPage
    {
        PhoneNumberChooserTask phoneNumberChooserTask;
        public Page2()
        {
            InitializeComponent();
            
        }
        
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }
        private void GestureListener_Flick(object sender, FlickGestureEventArgs e)
        {
            //ignore if direction is not horizontal 
            if (e.Direction != System.Windows.Controls.Orientation.Horizontal)
                return;

            
            //right-to-left
            if (e.HorizontalVelocity < 0)
            {
                //MessageBox.Show("right to left");
                string uriString = "/Page3.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));

            }

            //left-to-right
            if (e.HorizontalVelocity > 0)
            {
                //MessageBox.Show("left to right");
                string uriString = "/Page1.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));
            }
        }

        private void Button_clicked(object sender, RoutedEventArgs e)
        {
            Button current_button = (Button)sender;
            String button_name = current_button.Name;
            switch (button_name)
            {
                case "calculator":
                    break;
                case "contacts":
                    PhoneNumberChooserTask address = new PhoneNumberChooserTask();
                    address.Show();
                    break;
                case "clock":
                    break;
                case "facebook":
                    WebBrowserTask webBrowserTask1 = new WebBrowserTask();
                    webBrowserTask1.Uri = new Uri("http://www.facebook.com", UriKind.Absolute);
                    webBrowserTask1.Show();
                    break;
                
                case "phone":
                    phoneNumberChooserTask = new PhoneNumberChooserTask();
                    phoneNumberChooserTask.Completed += new EventHandler<PhoneNumberResult>(phoneNumberChooserTask_Completed);
                    try
                    {
                        phoneNumberChooserTask.Show();
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        MessageBox.Show("An error occurred.");
                    }
                    //PhoneCallTask phoneCallTask = new PhoneCallTask();

                    //phoneCallTask.PhoneNumber = "123456789";
                    //phoneCallTask.DisplayName = "Shadowfax Apps";

                    //phoneCallTask.Show();
                    break;
                case "mail":
                    EmailComposeTask emailComposeTask = new EmailComposeTask();
                    emailComposeTask.Subject = "";
                    emailComposeTask.Body = "";
                    emailComposeTask.To = "";
                    emailComposeTask.Show();
                    break;
                case "safari":
                    WebBrowserTask webBrowserTask4 = new WebBrowserTask();
                    webBrowserTask4.Uri = new Uri("http://www.google.com", UriKind.Absolute);
                    webBrowserTask4.Show();
                    break;
                case "ipod":

                    break;

                default:
                    break;
            }
        }
        void phoneNumberChooserTask_Completed(object sender, PhoneNumberResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                MessageBox.Show("The phone number for " + e.DisplayName + " is " + e.PhoneNumber);

                //Code to start a new call using the retrieved phone number.
                PhoneCallTask phoneCallTask = new PhoneCallTask();
                phoneCallTask.DisplayName = e.DisplayName;
                phoneCallTask.PhoneNumber = e.PhoneNumber;
                phoneCallTask.Show();
            }
        }
    }
}