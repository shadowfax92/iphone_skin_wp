﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.IO.IsolatedStorage;

namespace iPhone_Skin
{
    public partial class Page3 : PhoneApplicationPage
    {
        IsolatedStorageSettings settings1 = IsolatedStorageSettings.ApplicationSettings;
        PhoneNumberChooserTask phoneNumberChooserTask;
        public Page3()
        {
            InitializeComponent();
        }
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            //string first_time_page3 = "";
            //if (!settings1.Contains("first_time_page3"))
            //{
            //    MessageBox.Show("Ads are added to support the future developement of this App and our other famous apps like \"Search All\". Please do cooperate with us!");
            //    settings1["first_time_page3"] = "first_time_page3_done";

            //}//end if
            if (Advertisement_Grid.Visibility == System.Windows.Visibility.Collapsed && Advertisement_Grid!=null)
            {
                Advertisement_Grid.Visibility = System.Windows.Visibility.Visible;
            }

        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }
        private void GestureListener_Flick(object sender, FlickGestureEventArgs e)
        {
            //ignore if direction is not horizontal 
            if (e.Direction != System.Windows.Controls.Orientation.Horizontal)
                return;
            
            //right-to-left
            if (e.HorizontalVelocity < 0)
            {
                //MessageBox.Show("right to left");
                string uriString = "/Page1.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));

            }

            //left-to-right
            if (e.HorizontalVelocity > 0)
            {
                //MessageBox.Show("left to right");
                string uriString = "/Page2.xaml";
                NavigationService.Navigate(
                new Uri(uriString, UriKind.RelativeOrAbsolute));
            }
        }

        private void Button_clicked(object sender, RoutedEventArgs e)
        {
            Button current_button = (Button)sender;
            String button_name = current_button.Name;
            switch (button_name)
            {
                case "phone":
                    phoneNumberChooserTask = new PhoneNumberChooserTask();
                    phoneNumberChooserTask.Completed += new EventHandler<PhoneNumberResult>(phoneNumberChooserTask_Completed);
                    try
                    {
                        phoneNumberChooserTask.Show();
                    }
                    catch (System.InvalidOperationException ex)
                    {
                        MessageBox.Show("An error occurred.");
                    }
                    //PhoneCallTask phoneCallTask = new PhoneCallTask();

                    //phoneCallTask.PhoneNumber = "123456789";
                    //phoneCallTask.DisplayName = "Shadowfax Apps";

                    //phoneCallTask.Show();
                    break;
                case "feedback":
                    EmailComposeTask emailComposeTask = new EmailComposeTask();
                    emailComposeTask.Subject = "iPhone Skip App feedback";
                    emailComposeTask.Body = "";
                    emailComposeTask.To = "shadowfax.apps@gmail.com";
                    emailComposeTask.Show();
                    break;
                case "website":
                    WebBrowserTask webBrowserTask4 = new WebBrowserTask();
                    webBrowserTask4.Uri = new Uri("http://shadowfax-apps.blogspot.in/", UriKind.Absolute);
                    webBrowserTask4.Show();
                    break;
                case "rate":
                    MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
                    marketplaceReviewTask.Show();

                    break;

                default:
                    break;
            }

        }
        void phoneNumberChooserTask_Completed(object sender, PhoneNumberResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                MessageBox.Show("The phone number for " + e.DisplayName + " is " + e.PhoneNumber);

                //Code to start a new call using the retrieved phone number.
                PhoneCallTask phoneCallTask = new PhoneCallTask();
                phoneCallTask.DisplayName = e.DisplayName;
                phoneCallTask.PhoneNumber = e.PhoneNumber;
                phoneCallTask.Show();
            }
        }

        
    }
}